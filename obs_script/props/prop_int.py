import obspython as obs

from obs_script.props.prop import ObsScriptProp


class ObsScriptPropInt(ObsScriptProp):
    OBS_LIST_ADD_FUNC = obs.obs_property_list_add_int
    OBS_LIST_FORMAT = obs.OBS_COMBO_FORMAT_INT

    def _make_prop(self, props):
        obs.obs_properties_add_int(
            props, self.name,
            self.description,
            self.min, self.max, self.step
        )

    def __init__(self, name, default_value,
                 min_val,
                 max_val,
                 step,
                 description="",
                 list_el_generator=None
                 ):
        super().__init__(
            name, default_value,
            description,
            list_el_generator=list_el_generator
        )
        self.min = min_val
        self.max = max_val
        self.step = step

    def init_default(self, props):
        obs.obs_data_set_default_int(props, self.name, self.default_value)

    def get(self, settings):
        return obs.obs_data_get_int(settings, self.name)
