from typing import NamedTuple

import obspython as obs

ListPropDefinition = NamedTuple("ListPropDefinition", [
    ("name", "description")
])


class ObsScriptProp(object):
    """
    Script properties will be visible in the script config UI
    """
    OBS_LIST_ADD_FUNC = None
    OBS_LIST_FORMAT = None

    def __init__(
            self,
            name, default_value,
            description=None,
            list_el_generator=None
    ):
        """

        :param name:
        :type name:
        :param default_value:
        :type default_value:
        :param description:
        :type description:
        :param list_el_generator:
        :type list_el_generator: callable[ListPropDefinition]
        """
        self.default_value = default_value
        self._value = default_value
        self._value_old = default_value
        self.name = name
        self.description = description
        self.list_el_generator = list_el_generator

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value_old = self._value
        self._value = value

    @property
    def value_old(self):
        return self._value_old

    @property
    def changed(self):
        return self.value != self.value_old

    def get(self, settings):
        pass

    def update(self, settings):
        self.value = self.get(settings)

    def init_setting(self, props):
        if self.list_el_generator:
            self._make_list(props)
        else:
            self._make_prop(props)

    def _make_list(self, props):
        list_prop = self._make_list_prop(props)
        for name, value in self.list_el_generator():
            self._make_list_el_prop(list_prop, name, value)

    def _make_list_prop(self, props):
        if not self.OBS_LIST_FORMAT:
            raise ValueError("Please define OBS_LIST_FORMAT")
        return obs.obs_properties_add_list(
            props, self.name, self.description,
            obs.OBS_COMBO_TYPE_EDITABLE,
            self.OBS_LIST_FORMAT
        )

    def _make_list_el_prop(self, props, name, value):
        if not self.OBS_LIST_ADD_FUNC:
            raise ValueError("Please define OBS_LIST_ADD_FUNC")
        self.OBS_LIST_ADD_FUNC(props, name, value)

    def _make_prop(self, props):
        raise NotImplementedError()

    def init_default(self, props):
        raise NotImplementedError()
