import obspython as obs

from obs_script.props.prop import ObsScriptProp


class ObsScriptPropText(ObsScriptProp):
    OBS_LIST_ADD_FUNC = obs.obs_property_list_add_string
    OBS_LIST_FORMAT = obs.OBS_COMBO_FORMAT_STRING

    def __init__(self, name, default_value,
                 description=None,
                 list_el_generator=None,
                 _type=obs.OBS_TEXT_DEFAULT
                 ):
        super().__init__(
            name, default_value,
            description=description,
            list_el_generator=list_el_generator
        )
        self.obs_type = _type

    def _make_prop(self, props):
        obs.obs_properties_add_text(
            props, self.name,
            self.description,
            self.obs_type
        )

    def init_default(self, props):
        obs.obs_data_set_default_string(props, self.name, self.default_value)

    def get(self, settings):
        return obs.obs_data_get_string(settings, self.name)
