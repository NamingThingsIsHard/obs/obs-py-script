import obspython as obs


class ObsScript(object):
    PROPERTY_PREFIX = "_prop"
    SETTING_PREFIX = "_setting"

    def script_defaults(self, settings):
        for name, value in self._iter_props():
            value.init_default(settings)

    def script_description(self):
        return ""

    def script_load(self, settings):
        """
        Called on script startup with specific settings associated with the script.

        By default we will load all the user defined settings
         and custom script settings

        The settings parameter provided is not typically used for
         settings that are set by the user;
         instead the parameter is used for any extra
         internal settings data that may be used in the script.

        https://obsproject.com/docs/scripting.html#script_load
        """
        for name, value in self._iter_props():
            value.get(settings)
        for name, value in self._iter_props(self.SETTING_PREFIX):
            value.get(settings)

    def script_properties(self):
        props = obs.obs_properties_create()
        self.init_settings(props)
        return props

    def script_update(self, settings):
        for name, value in self._iter_props():
            value.update(settings)

    def script_save(self, settings):
        """
        Called when the script is being saved. This is not necessary for
         settings that are set by the user; instead this is used for
         any extra internal settings data that may be used in the script.

        https://obsproject.com/docs/scripting.html#script_save
        :param settings:
        :type settings:
        """

    def script_unload(self):
        """
        Called when the script is being unloaded.

        https://obsproject.com/docs/scripting.html#script_unload
        """

    def _iter_props(self, prefix=PROPERTY_PREFIX):
        """
        :return:
        :rtype: tuple[basestring, ObsScriptProp]
        """
        for name, value in vars(self).items():
            if not name.startswith(prefix):
                continue
            yield name, value

    def init_settings(self, settings):
        for name, value in self._iter_props():
            value.init_setting(settings)


