import functools

import obspython as obs
from twitch import TwitchClient

from obs_script import ObsScript
from obs_script.props.prop_int import ObsScriptPropInt
from obs_script.props.prop_text import ObsScriptPropText
from utils import monkey_patch_this, list_sources


def _build_prop_list(*types):
    ret_list = []
    for source in list_sources(*types):
        name = obs.obs_source_get_name(source)
        ret_list.append((name, name))
    return ret_list


class ObsTwitchScript(ObsScript):
    VERSION = "0.1"

    def __init__(self):
        self._prop_interval = ObsScriptPropInt("interval", 10, 1, 3600, 1, "Interval")
        self._prop_media_source_name = ObsScriptPropText(
            "media_source_name", "", "Media Source",
            functools.partial(_build_prop_list, "ffmpeg_source")
        )
        self._prop_text_source_name = ObsScriptPropText(
            "text_source", "", "Text Source",
            functools.partial(_build_prop_list, "text_gdiplus", "text_ft2_source")
        )
        self._prop_client_id = ObsScriptPropText("client_id", "", "Client ID")
        self._prop_oauth_token = ObsScriptPropText("oauth_token", "", "OAuth Token", _type=obs.OBS_TEXT_PASSWORD)
        self._setting_channel_name = ObsScriptPropText("channel_name", "")
        self._client = TwitchClient()

    def script_description(self):
        return """
        <h1>OBS-Twitch integration v%(version)s</h1>
        <p>Integrates some twitch functions into OBS</p>
        <h2>Features</h2>
        <ul>
            <li>Follower notification</li>
        </ul>
        
        <hr/>
        <p>
        Create your Client-ID here: 
            <a href="https://dev.twitch.tv/console/apps/create">
                Twitch Dev
            </a>
        </p>
        <p>
            Create your Oauth-Token here 
            (you need channel_read and channel_editor permission):
            <a href="https://twitchtokengenerator.com/quick/8hkFXMYaO0">
                twitchtokengenerator.com
            </a>
        </p>
        """ % {
            "version": self.VERSION
        }

    def script_update(self, settings):
        super(ObsTwitchScript, self).script_update(settings)

        obs.timer_remove(self._check_follows)

        twitch_change = self._prop_client_id.changed \
                        or self._prop_oauth_token.changed
        obs.script_log(obs.LOG_INFO, "Twitch change %s" % twitch_change)
        if twitch_change and self._validate_twitch_settings():
            obs.timer_add(self._check_follows, self._prop_interval.value * 1000)

    def _validate_twitch_settings(self):
        self._client = TwitchClient(
            client_id=self._prop_client_id.value,
            oauth_token=self._prop_oauth_token.value
        )
        res = None
        try:
            res = self._client.users.get()
            self._setting_channel_name.value = res.name
            obs.script_log(obs.LOG_INFO, "Twitch settings validated for user: %s" % self._setting_channel_name.value)
        except Exception as e:
            obs.script_log(obs.LOG_WARNING, "Couldn't validate twitch settings: %s" % e)
            raise
        return res is not None

    def _check_follows(self):
        pass

    def _display_follow(self, user):
        source = obs.obs_get_source_by_name(self._prop_media_source_name.value)
        obs.script_log(obs.LOG_INFO, "Getting for source %s" % self._prop_media_source_name.value)
        if source is not None:
            # Updating the settings forces an active ffmpeg_source to play
            settings = obs.obs_source_get_settings(source)
            obs.obs_source_update(source, settings)


s = ObsTwitchScript()
monkey_patch_this(globals(), s)
