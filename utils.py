from inspect import ismethod

import obspython as obs


def monkey_patch_this(g, o):
    """
    Make the given module an OBS-Script

    Defines / binds all necessary methods to the current module
    for OBS to be able to run it

    see: https://obsproject.com/docs/scripting.html#script-function-exports

    :param g: globals of a module
    :type g: dict
    :type o: obs_script.ObsScript
    """

    # g = globals()
    for name in dir(o):
        value = getattr(o, name)
        if not (name.startswith("script_") and ismethod(value)):
            continue
        g[name] = value


def list_sources(*types):
    # TODO: move to a module
    sources = obs.obs_enum_sources()
    if sources is not None:
        for source in sources:
            source_id = obs.obs_source_get_id(source)
            if source_id in types:
                yield source

        obs.source_list_release(sources)
