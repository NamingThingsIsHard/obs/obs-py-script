import unittest
from unittest.mock import patch

from obs_script import ObsScript
from obs_script.props.prop import ObsScriptProp
from utils import monkey_patch_this


class ObsScriptTest(unittest.TestCase):
    def test_init(self):
        class ObsScriptPropTest(ObsScriptProp):
            def init_setting(self, props):
                self.lol = True

        class TestObsScript(ObsScript):
            def __init__(self):
                self._prop_int = ObsScriptPropTest("my_int", "test")

        test_script = TestObsScript()
        with patch.object(test_script._prop_int, "init_setting",
                          side_effect=test_script._prop_int.init_setting) as mock_method:
            test_script.init_settings({})

        mock_method.assert_called()
        self.assertTrue(test_script._prop_int.lol)

    def test_monkey_patch(self):
        o = ObsScript()
        monkey_patch_this(globals(), o)
        g = globals()
        self.assertTrue("script_load" in g)
        g["script_load"]("something")


if __name__ == '__main__':
    unittest.main()
